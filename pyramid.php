// Writing a pyramid to a file
<?php
$totalLines = 100;
$fp = fopen("pyramid.txt","w+");
for($i=1;$i<=$totalLines;$i++)
{
    for($sp=$totalLines-$i; $sp>0 ; $sp-- ){
        fwrite($fp," ");
    }//emp1 loop who prints spaces
    for($star=1; $star<= ($i*2)-1 ; $star++ )
    {
        fwrite($fp,"*");
    }//emp2
    fwrite($fp,"\n");
}// Boss For Loop
?>